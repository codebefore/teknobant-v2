var app = angular.module('SocialApp', []);
app.controller('SocialCtrl', function ($scope, $http) {
    $("#social").addClass("active");
    $scope.looder = true;
    var index = 0;
    getSocials();
    function getSocials() {
        $http({
            method: "GET",
            url: "/Admin/GetSocials"
        }).then(function success(response) {
            $scope.model = response.data.model;
            $scope.looder = false;
            alertify.success(response.data.message);
        },
            function error(reason) {
                $scope.looder = false;
                $scope.error = reason.data.message;
                alertify.error(reason.data.message);

            });
    }
    function getSelectIndex(id) {
        for (var i = 0; i < $scope.model.length; i++) {
            if ($scope.model[i].id === id) {
                return i;
            }
        }
    }
    function clear() {
        $scope.name = undefined;
        $scope.link = undefined;
        $scope.icon = undefined;
    }

    $scope.openAdd = function () {
        $("#addModal").modal("show");
    };
    $scope.add = function () {

        var item = {
            name: $scope.name,
            link: $scope.link,
            icon: $scope.icon,
        };

        console.log(item);
        $http({
            method: "POST",
            url: "/Admin/AddSocial/",
            data: JSON.stringify(item)
        }).then(function success(response) {
            $scope.model.push(response.data.model);
            clear();
            $("#addModal").modal("hide");
            alertify.success(response.data.message);


        }, function unsuccess(reason) {
            alertify
                .delay(5000)
                .error(reason.data);
        });

    };
    $scope.update = function () {

        index = getSelectIndex($scope.id);
        $scope.model[index].name = $scope.name;
        $scope.model[index].link = $scope.link;
        $scope.model[index].icon = $scope.icon;
        $http({
            method: "POST",
            url: "/Admin/UpdateSocial",
            data: $scope.model[index]
        }).then(function success(response) {
            alertify.success(response.data.message);
            $("#editModal").modal("hide");
            clear();

        }, function unsuccess(response) {
            alertify.error(response.data.message);
        });


    };
    $scope.edit = function (id) {
        $("#editModal").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.name = $scope.model[index].name;
        $scope.link = $scope.model[index].link;
        $scope.icon = $scope.model[index].icon;
    };
    $scope.isDelete = function (id) {
        $("#ModalDelete").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.name = $scope.model[index].name;
        $scope.item = $scope.model[index];
    };
    $scope.delete = function () {

        $http({
            method: "POST",
            url: "/Admin/DeleteSocial",
            data: $scope.item
        }).then(function success(response) {
            index = getSelectIndex($scope.id);
            $scope.model.splice(index, 1);
            alertify.success(response.data.message);
            clear();
        }, function unsuccess(response) {

            alertify.error(response.data.message);
        });


    };
});

$(function () {
    $("#sortable").sortable({
        stop: function (event, ui) {
            var data = {
                items: []
            };
            $("#sortable li").each(function () {

                data.items.push({
                    "Id": $(this).find(".spanId").html(),
                    "OrderNo": $(this).find(".spanOrderNo").html(),
                });

            });
            //var finalval = JSON.stringify(data);
            //alert(finalval);
            $.ajax({
                type: "POST",
                url: "/Admin/UpdateSocialOrder",
                data: JSON.stringify(data.items),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //window.location.reload(); Sayfa refresh
                    console.log(response.message);
                    alertify.success(response.message);

                },
                error: function (reason) {
                    alertify.error(reason.message);
                }
            });
        }
    });
    $("#sortable").disableSelection();
});