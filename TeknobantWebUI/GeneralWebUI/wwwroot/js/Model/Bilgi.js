﻿var app = angular.module('bilgiApp', []);
app.controller('bilgiCtrl', function ($scope, $http) {
    


    $scope.pleaseWait = false;
    $scope.message = undefined;

    function clear() {
        $scope.email = undefined;
        $scope.name = undefined;
        $scope.phone = undefined;
        $scope.subject = undefined;
        $scope.object = undefined;
    }
    

    $scope.sendEMail = function () {
        $scope.pleaseWait = true;
        if ($scope.email != undefined || $scope.name != undefined || $scope.phone != undefined) {
            var item = {
                email: $scope.email,
                phone: $scope.phone,
                name: $scope.name,
                subject: $scope.subject,
                message: $scope.object,
            };
            $http({
                method: "POST",
                url: "/SendMail/",
                data: JSON.stringify(item)
            }).then(function success(response) {
                $scope.message = response.data.message;
                $scope.pleaseWait = false;
                clear();

            }, function error(response) {
                $scope.message = response.data.message;
                $scope.pleaseWait = false;
            });
        }
        else {
            $scope.pleaseWait = false;
            $scope.message = "Lütfen boş bırakmayın !";

        }
    };

   

});