
var app = angular.module('ReferenceApp', ['uiCropper']);
app.directive('ngFiles', ['$parse', function ($parse) {
    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);

        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });


    }

    return {
        link: fn_link
    }
}]);
app.controller('ReferenceCtrl', function ($scope, $http) {
    $("#reference").addClass("active");

    $scope.looder = true;
    var index = 0;
    getReferences();
    function getReferences() {
        $http({
            method: "GET",
            url: "/Admin/GetReferences"
        }).then(function success(response) {
            $scope.model = response.data.model;
            console.log($scope.model);
            $scope.looder = false;
        },
            function error(reason) {
                $scope.looder = false;
                $scope.error = reason.data.message;
                alertify.error(reason.data.message);

            });
    }
    function getSelectIndex(id) {
        for (var i = 0; i < $scope.model.length; i++) {
            if ($scope.model[i].id === id) {
                return i;
            }
        }
    }
    function clear() {
        $scope.localUrl = undefined;
        $scope.cloudUrl = undefined;
        $scope.publicId = undefined;
        $scope.alt = undefined;
        $scope.orderNo = 0;
        $scope.isDeleted = false;
    }

    $scope.openAdd = function () {
        $("#addModal").modal("show");
    };
    $scope.add = function () {
        var item = {
            localUrl: $scope.localUrl,
            cloudUrl: $scope.cloudUrl,
            publicId: $scope.publicId,
            alt: $scope.alt
        };

        $http({
            method: "POST",
            url: "/Admin/AddReference/",
            data: JSON.stringify(item)
        }).then(function success(response) {
            $scope.model.push(response.data.model);
            $scope.looding = false;
            $scope.isLood = false;
            $("#addModal").modal("hide");
            clear();
            alertify.success(response.data.message);
            $scope.reset();
        }, function unsuccess(reason) {
            $scope.isLood = false;

            alertify
                .delay(5000)
                .error(reason.data.message);
        });

    };
    $scope.update = function () {

        index = getSelectIndex($scope.id);
        $scope.model[index].alt = $scope.alt;
        $scope.model[index].publicId = $scope.publicId;
        $scope.model[index].orderNo = $scope.orderNo;
        $scope.model[index].isDeleted = $scope.isDeleted;
        $scope.model[index].localUrl = $scope.localUrl;
        $scope.model[index].cloudUrl = $scope.cloudUrl;
        $http({
            method: "POST",
            url: "/Admin/UpdateReference",
            data: $scope.model[index]
        }).then(function success(response) {
            alertify.success(response.data.message);
            $("#editModal").modal("hide");
            clear();

        }, function unsuccess(response) {
            alertify.error(response.data.message);
        });


    };
    $scope.edit = function (id) {
        $("#editModal").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.alt = $scope.model[index].alt;
        $scope.publicId = $scope.model[index].publicId;
        $scope.localUrl = $scope.model[index].localUrl;
        $scope.cloudUrl = $scope.model[index].cloudUrl;
        $scope.orderNo = $scope.model[index].orderNo;
        $scope.isDeleted = $scope.model[index].isDeleted;
    };
    $scope.isDelete = function (id) {
        $("#ModalDelete").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.alt = $scope.model[index].alt;
        $scope.item = $scope.model[index];
    };
    $scope.delete = function () {

        $http({
            method: "POST",
            url: "/Admin/DeleteReference",
            data: $scope.item
        }).then(function success(response) {
            index = getSelectIndex($scope.id);
            $scope.model.splice(index, 1);
            alertify.success(response.data.message);
            clear();
        }, function unsuccess(response) {

            alertify.error(response.data.message);
        });


    };



    var formdata = new FormData();
    $scope.getTheFiles = function ($files) {
        $scope.imagesrc = [];
        for (var i = 0; i < $files.length; i++) {
            var reader = new FileReader();
            reader.filename = $files[i].name;
            reader.onload = function (event) {
                var image = {};
                image.name = event.target.name;
                //image.size = (event.total / 1024).toFixed(2);
                image.Src = event.target.result;
                $scope.imagesrc.push(image);
                $scope.FileName = image.Src;
                $scope.$apply();
            };

            reader.readAsDataURL($files[i]);
        }

        angular.forEach($files, function (value, key) {
            formdata.append(key, value);

        });
    };
    $scope.isUpload = false;
    $scope.uploadFiles = function () {
        $scope.imageLooderMessage = "Lutfen bekleyiniz";
        $scope.imageLooder = true;
        $scope.ImgLoaded = false;
        $scope.ImgLooding = true;
        alertify.delay(9000).log('Yukleniyor...');

        $http({
            method: "POST",
            url: "/Admin/UploadFile",
            data: JSON.stringify($scope.myCroppedImage)
        }).then(function success(response) {
            alertify
                .delay(5000)
                .success(response.data.message);
            $scope.isUpload = true;
            $scope.isAdd = true;

            $scope.reset();
            $scope.ImgLoaded = true;
            $scope.ImgLooding = false;
            $scope.imageLooder = false;
            $scope.imageLooderMessage = undefined;


        }, function error(reason) {
            $scope.ImgLooding = false;
            $scope.imageLooder = false;

            console.log(reason);
            alertify
                .delay(5000)
                .error(reason.data.message);
            $scope.reset();
        });


    };
    $scope.reset = function () {
        angular.forEach(
            angular.element["input [type = 'file']"], function (inputElem) {
                angular.element(inputElem).val(null);
            }
        );
        $scope.imagesrc = [];
        formdata = new FormData();
    };

    $scope.myImage = '';
    $scope.myCroppedImage = '';
    var handleFileSelect = function (evt) {
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function ($scope) {
                $scope.myImage = evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);

});
$(function () {
    $("#sortable").sortable({
        stop: function (event, ui) {
            var data = {
                items: []
            };
            $("#sortable li").each(function () {

                data.items.push({
                    "Id": $(this).find(".spanId").html(),
                    "OrderNo": $(this).find(".spanOrderNo").html()
                });

            });
            //var finalval = JSON.stringify(data);
            //alert(finalval);
            $.ajax({
                type: "POST",
                url: "/Admin/UpdateReferenceOrder",
                data: JSON.stringify(data.items),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //window.location.reload(); Sayfa refresh
                    console.log(response.message);
                    alertify.success(response.message);

                },
                error: function (reason) {
                    alertify.error(reason.message);
                }
            });
        }
    });
    $("#sortable").disableSelection();
});