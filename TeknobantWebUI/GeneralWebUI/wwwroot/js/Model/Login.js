﻿var app = angular.module('LoginApp', []);
app.controller('LoginCtrl', function ($scope, $http) {
    $scope.PassControl = false;


    $scope.LoginControl = function () {
        $scope.PassControl = true;
        var item = {
            email: $scope.email,
            password: $scope.pass
        };
        $http({
            method: "POST",
            url: "/Admin/LoginControl/",
            data: JSON.stringify(item)
        }).then(function success(response) {
            if (response.data.success) {
                window.location.href = "/Admin/Users";
            }
            else {
                $scope.message = response.data.message;
                $scope.PassControl = false;
            }
        }, function error(response) {
            console.log(response);
           
            $scope.message = response.data.message;
        });

    };
});