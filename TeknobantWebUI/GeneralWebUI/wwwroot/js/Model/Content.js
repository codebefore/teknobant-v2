var app = angular.module('ContentApp', ['summernote']);

app.controller('ContentCtrl', function ($scope, $http) {
    $("#content").addClass("active");
    $scope.looder = true;
    var index = 0;
    getContents();
    function getContents() {
        $http({
            method: "GET",
            url: "/Admin/GetContents"
        }).then(function success(response) {
            $scope.model = response.data.model;
            $scope.looder = false;
            alertify.success(response.data.message);
        },
            function error(reason) {
                $scope.looder = false;
                $scope.error = reason.data.message;
                alertify.error(reason.data.message);

            });
    }
    function getSelectIndex(id) {
        for (var i = 0; i < $scope.model.length; i++) {
            if ($scope.model[i].id === id) {
                return i;
            }
        }
    }
    function clear() {
        $scope.name = undefined;
        $scope.description = undefined;
        $scope.shortDescription = undefined;
        $scope.nameEn = undefined;
        $scope.descriptionEn = undefined;
        $scope.shortDescriptionEn = undefined;
        $scope.nameGe = undefined;
        $scope.descriptionGe = undefined;
        $scope.shortDescriptionGe = undefined;
    }

    $scope.openAdd = function () {
        $("#addModal").modal({ backdrop: 'static', keyboard: false });
    };
    $scope.add = function () {

        var item = {
            name: $scope.name,
            description: $scope.description,
            shortDescription: $scope.shortDescription,
            nameEn: $scope.nameEn,
            descriptionEn: $scope.descriptionEn,
            shortDescriptionEn: $scope.shortDescriptionEn,
            nameGe: $scope.nameGe,
            descriptionGe: $scope.descriptionGe,
            shortDescriptionGe: $scope.shortDescriptionGe
        };

        console.log(item);
        $http({
            method: "POST",
            url: "/Admin/AddContent/",
            data: JSON.stringify(item)
        }).then(function success(response) {
            $scope.model.push(response.data.model);
            clear();
            $("#addModal").modal("hide");
            alertify.success(response.data.message);


        }, function unsuccess(reason) {
            alertify
                .delay(5000)
                .error(reason.data);
        });

    };
    $scope.update = function () {

        index = getSelectIndex($scope.id);
        $scope.model[index].name = $scope.name;
        $scope.model[index].description = $scope.description;
        $scope.model[index].shortDescription = $scope.shortDescription;
        $scope.model[index].nameEn = $scope.nameEn;
        $scope.model[index].descriptionEn = $scope.descriptionEn;
        $scope.model[index].shortDescriptionEn = $scope.shortDescriptionEn;
        $scope.model[index].nameGe = $scope.nameGe;
        $scope.model[index].descriptionGe = $scope.descriptionGe;
        $scope.model[index].shortDescriptionGe = $scope.shortDescriptionGe;
        $http({
            method: "POST",
            url: "/Admin/UpdateContent",
            data: $scope.model[index]
        }).then(function success(response) {
            alertify.success(response.data.message);
            $("#editModal").modal("hide");
            clear();

        }, function unsuccess(response) {
            alertify.error(response.data.message);
        });


    };
    $scope.edit = function (id) {
        $("#editModal").modal({ backdrop: 'static', keyboard: false });
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.name = $scope.model[index].name;
        $scope.description = $scope.model[index].description;
        $scope.shortDescription = $scope.model[index].shortDescription;
        $scope.nameEn = $scope.model[index].nameEn;
        $scope.descriptionEn = $scope.model[index].descriptionEn;
        $scope.shortDescriptionEn = $scope.model[index].shortDescriptionEn;
        $scope.nameGe = $scope.model[index].nameGe;
        $scope.descriptionGe = $scope.model[index].descriptionGe;
        $scope.shortDescriptionGe = $scope.model[index].shortDescriptionGe;
    };
    $scope.isDelete = function (id) {
        $("#ModalDelete").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.name = $scope.model[index].name;
        $scope.item = $scope.model[index];
    };
    $scope.delete = function () {

        $http({
            method: "POST",
            url: "/Admin/DeleteContent",
            data: $scope.item
        }).then(function success(response) {
            index = getSelectIndex($scope.id);
            $scope.model.splice(index, 1);
            alertify.success(response.data.message);
            clear();
        }, function unsuccess(response) {
            alertify.error(response.data.message);
        });


    };


    
});
function closeModal() {
    $(".closer").modal("hide");
}
$(function () {
    $("#sortable").sortable({
        stop: function (event, ui) {
            var data = {
                items: []
            };
            $("#sortable li").each(function () {

                data.items.push({
                    "Id": $(this).find(".spanId").html(),
                    "OrderNo": $(this).find(".spanOrderNo").html()
                });

            });
            //var finalval = JSON.stringify(data);
            //alert(finalval);
            $.ajax({
                type: "POST",
                url: "/Admin/UpdateContentOrder",
                data: JSON.stringify(data.items),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //window.location.reload(); Sayfa refresh
                    console.log(response.message);
                    alertify.success(response.message);

                },
                error: function (reason) {
                    alertify.error(reason.message);
                }
            });
        }
    });
    $("#sortable").disableSelection();
});