﻿using GeneralWebUI.Model.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class LoginContext:DbContext
    {
        public static string ConnectionString { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }
        public DbSet<Login> Login { get; set; }
        public DbSet<Slider> Slider { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Content> Content { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductCategory> ProductCategory { get; set; }
        public DbSet<ProductImage> ProductImage { get; set; }
        public DbSet<Reference> Reference { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<Social> Social { get; set; }
        public DbSet<Meta> Meta { get; set; }
        public DbSet<Counter> Counter { get; set; }
        public DbSet<Service> Service { get; set; }
        public DbSet<UploadImage> UploadImage { get; set; }
    }
}
