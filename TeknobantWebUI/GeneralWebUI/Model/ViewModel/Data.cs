﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model.ViewModel
{
    public class Data
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string Url { get; set; }
        public object Model { get; set; }
        public bool Success { get; set; }
    }
}
