﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model.ViewModel
{
    public class MenuViewModel
    {
        public List<Menu> Menus { get; set; }
        public List<ProductCategory> ProductCategories { get; set; }
        public List<Contact> Contacts { get; set; }
        public List<Social> Socials { get; set; }
    }
}
