﻿using GeneralWebUI.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model.ViewModel
{
    public class GeneralViewModel
    {
        public List<Slider> Sliders { get; set; }
        public List<Product> Products { get; set; }
        public List<ProductCategory> ProductCategories { get; set; }
        public List<Content> Contents { get; set; }
        public List<Contact> Contacts { get; set; }
        public List<Social> Socials { get; set; }
        public List<Visitor> Visitors { get; set; }
        public List<Reference> References { get; set; }
        public List<Counter> Counters { get; set; }
        public List<Service> Services { get; set; }
        public List<Duyuru> Duyurus { get; set; }
        public Content Content { get; set; }
        public Service Service { get; set; }

    }
}
