﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model.Entity
{
    public class Reference : BaseEntity
    {
        public string LocalUrl { get; set; }
        public string CloudUrl { get; set; }
        public string PublicId { get; set; }
        public string Alt { get; set; }
    }
}
