﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class Contact:BaseEntity
    {
        public string Name { get; set; }
        public string Map { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string AddressEn { get; set; }
        public string AddressGe { get; set; }
        public string Cep { get; set; }


    }
}
