﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class Counter:BaseEntity
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public int Number { get; set; }
    }
}
