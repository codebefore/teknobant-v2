﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model.Entity
{
    public class Slider : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string NameEn { get; set; }
        public string DescriptionEn { get; set; }
        public string NameGe { get; set; }
        public string DescriptionGe { get; set; }
        public string LocalUrl { get; set; }
        public string CloudUrl { get; set; }
        public string PublicId { get; set; }
    }
}
