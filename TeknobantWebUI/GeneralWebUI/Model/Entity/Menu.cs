﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class Menu:BaseEntity
    {
        
        public string Name { get; set; }
        public string NameEn { get; set; }
        public string NameGe { get; set; }
        public string Link { get; set; }
        public string LinkEn { get; set; }
        public string LinkGe { get; set; }
    }
}
