﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class Meta:BaseEntity
    {
        
        public string Title { get; set; }
        public string Author { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
    }
}
