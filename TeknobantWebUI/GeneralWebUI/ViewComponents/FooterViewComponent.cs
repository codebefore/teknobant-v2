﻿using GeneralWebUI.Model;
using GeneralWebUI.Model.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.ViewComponents
{
    public class FooterViewComponent : ViewComponent
    {
        readonly LoginContext _context;
        public FooterViewComponent(LoginContext context)
        {
            _context = context;
        }

        public ViewViewComponentResult Invoke()
        {
            MenuViewModel model = new MenuViewModel()
            {
                Contacts=_context.Contact.Where(x=> x.IsDeleted!=true)
                                         .OrderBy(x=> x.OrderNo)
                                         .ToList(),

                Menus=_context.Menu.Where(x => x.IsDeleted != true)
                                         .OrderBy(x => x.OrderNo)
                                         .ToList(),
                Socials = _context.Social.Where(x => x.IsDeleted != true)
                                         .OrderBy(x => x.OrderNo)
                                         .ToList()
            };

            return View(model);
        }
    }
}
