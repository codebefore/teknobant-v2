﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Helper
{
    public class ImageHelper
    {
        public static string CloudinaryUrl { get; set; }
        public static string ImageUrl { get; set; }
        public static string PublicId { get; set; }
    }
}
