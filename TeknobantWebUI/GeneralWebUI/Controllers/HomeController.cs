﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using GeneralWebUI.Model;
using GeneralWebUI.Model.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GeneralWebUI.Controllers
{
    [Route("")]
    [Route("/Anasayfa", Name = "Anasayfa")]
    [Route("/{lang}/Anasayfa", Name = "AnasayfaLang")]
    public class HomeController : Controller
    {
        LoginContext _context;
        public HomeController(LoginContext context)
        {
            _context = context;
        }

        public IActionResult Index(string lang)
        {
            GeneralViewModel model = new GeneralViewModel()
            {
                Sliders = _context.Slider.Where(x => x.IsDeleted != true)
                                        .OrderBy(x => x.OrderNo)
                                        .ToList(),

                Products = _context.Product.Where(x => x.IsDeleted != true && x.Images.Count > 0)
                                           .Include(x => x.Images)
                                           .Include(x => x.ProductCategory)
                                           .OrderBy(x => x.OrderNo)
                                           .ToList(),

                Contents = _context.Content.Where(x => x.IsDeleted != true)
                                          .OrderBy(x => x.OrderNo)
                                          .ToList(),

                Counters = _context.Counter.OrderBy(x => x.OrderNo)
                                           .Where(x => x.IsDeleted != true)
                                           .ToList(),

                Services = _context.Service.OrderBy(x => x.OrderNo)
                                         .Where(x => x.IsDeleted != true)
                                         .ToList()
            };

            Meta meta = _context.Meta.FirstOrDefault(x => x.Id == 1);

            ViewBag.Author = meta.Author;
            ViewBag.Description = meta.Description;
            ViewBag.Keywords = meta.Keywords;
            ViewBag.Title = meta.Title;
            ViewBag.Page = "/Anasayfa/";

            if (lang == null)
            {
                ViewBag.Dil = "tr";
            }
            else
            {
                ViewBag.Dil = lang;
            }
            return View(model);
        }

        [Route("/kurumsal.html", Name = "Kurumsalhtml")]
        [Route("/Kurumsal", Name = "Kurumsal")]
        [Route("/{lang}/Kurumsal", Name = "Kurumsallang")]
        public IActionResult Corporation(string lang)
        {
            GeneralViewModel model = new GeneralViewModel()
            {
                Contents = _context.Content.Where(x => x.IsDeleted != true)
                                        .OrderBy(x => x.OrderNo)
                                        .ToList()
            };

            Meta meta = _context.Meta.FirstOrDefault(x => x.Id == 2);

            ViewBag.Author = meta.Author;
            ViewBag.Description = meta.Description;
            ViewBag.Keywords = meta.Keywords;
            ViewBag.Title = meta.Title;
            ViewBag.Page = "/Kurumsal/";
            if (lang == null)
            {
                ViewBag.Dil = "tr";
            }
            else
            {
                ViewBag.Dil = lang;
            }
            return View(model);
        }

        [Route("/elektrikbantlari.html", Name = "elektrikbantlarihtml")]
        [Route("/maskelemebantlari.html", Name = "maskelemebantlarihtml")]
        [Route("/clothducttape.html", Name = "clothducttapehtml")]
        [Route("/pvcbantlar.html", Name = "pvcbantlarhtml")]
        [Route("/Ürünler", Name = "Ürünler")]
        [Route("/{lang}/Ürünler", Name = "Ürünlerlang")]
        [Route("/{lang}/Ürünler/{id}", Name = "ÜrünlerlangByCategory")]
        public IActionResult Products(string lang,int? id)
        {
            GeneralViewModel model;
            if (id>0)
            {
                 model = new GeneralViewModel()
                {
                    Products = _context.Product.Where(x => x.IsDeleted != true && 
                                                           x.Images.Count > 0  &&
                                                           x.ProductCategoryId==id)
                                           .Include(x => x.ProductCategory)
                                           .Include(x => x.Images)
                                           .OrderBy(x => x.OrderNo)
                                           .ToList(),
                    ProductCategories = _context.ProductCategory.Where(x => x.IsDeleted != true)
                                                .Include(x => x.Products)
                                                .OrderBy(x => x.OrderNo)
                                               .ToList(),
                };
            }
            else
            {
                 model = new GeneralViewModel()
                {
                    Products = _context.Product.Where(x => x.IsDeleted != true && x.Images.Count > 0)
                                                           .Include(x => x.ProductCategory)
                                                           .Include(x => x.Images)
                                                           .OrderBy(x => x.OrderNo)
                                                           .ToList(),
                    ProductCategories = _context.ProductCategory.Where(x => x.IsDeleted != true)
                                .Include(x => x.Products)
                                                           .OrderBy(x => x.OrderNo)
                                                           .ToList(),
                };
            }
            

            Meta meta = _context.Meta.SingleOrDefault(x => x.Id == 17);

            ViewBag.Author = meta.Author;
            ViewBag.Description = meta.Description;
            ViewBag.Keywords = meta.Keywords;
            ViewBag.Title = meta.Title;
            ViewBag.Page = "/Ürünler/";
            if (lang == null)
            {
                ViewBag.Dil = "tr";
            }
            else
            {
                ViewBag.Dil = lang;
            }

            return View(model);
        }

        [Route("/urun/{ürün}/{id}", Name = "ürün")]
        [Route("/{lang}/urun/{ürün}/{id}", Name = "Ürünlang")]
        public IActionResult ProductDetail(string lang, int id)
        {
            if (id == 0)
            {
                return RedirectToRoute("");
            }

            List<Product> plist = _context.Product.Where(x => x.IsDeleted != true && x.Images.Count > 0)
                                           .Include(x => x.Images)
                                           .Include(x => x.ProductCategory)
                                           .OrderBy(x => x.OrderNo)
                                           .ToList();

            if (plist.Count == 0)
            {
                return RedirectToRoute("");
            }

            Product p = plist.FirstOrDefault(x => x.Id == id);
            int index = plist.FindIndex(x => x.Id == id);
            int size = plist.Count() - 1;
            ProductViewModel model = new ProductViewModel()
            {
                Product = p,
                Products = plist,
                PreviousProduct = index == 0 ? plist.Last() : plist[index - 1],
                NextProduct = size == index ? plist.FirstOrDefault() : plist[index + 1],
            };

            Meta meta = _context.Meta.FirstOrDefault(x => x.Id == 17);
            ViewBag.Author = meta.Author;
            ViewBag.Description = meta.Description;
            ViewBag.Keywords = meta.Keywords;

            ViewBag.Page = "/urun/" + p.Name.Replace(" ", "").Replace("/", "").Replace(".", "").Replace("\\", "") + "/" + p.Id;
            if (lang == null)
            {
                ViewBag.Dil = "tr";
                ViewBag.Title = p.Name;
            }
            else
            {
                ViewBag.Dil = lang;
                ViewBag.Title = p.NameEn;
            }
            return View(model);
        }

        [Route("/Kalite", Name = "Kalite")]
        [Route("/{lang}/Kalite", Name = "Kalitelang")]
        public IActionResult References(string lang)
        {
           

            Meta meta = _context.Meta.FirstOrDefault(x => x.Id == 20);

            ViewBag.Author = meta.Author;
            ViewBag.Description = meta.Description;
            ViewBag.Keywords = meta.Keywords;
            ViewBag.Title = meta.Title;
            ViewBag.Page = "/Kalite";
            if (lang == null)
            {
                ViewBag.Dil = "tr";
            }
            else
            {
                ViewBag.Dil = lang;
            }
            return View();
        }

        [Route("/Sektörler", Name = "Sektörler")]
        [Route("/{lang}/Sektörler", Name = "Sektörlerlang")]
        [Route("/{lang}/Sektörler/{id}", Name = "SektörlerlangDetail")]
        public IActionResult Services(string lang, int? id)
        {
            GeneralViewModel model;
            if (id>0)
            {
                 model = new GeneralViewModel()
                {
                    Services = _context.Service.Where(x => x.IsDeleted != true)
                                                           .OrderBy(x => x.OrderNo)
                                                           .ToList(),
                    Service = _context.Service.SingleOrDefault(x => x.Id == id)
                };
            }
            else
            {
                 model = new GeneralViewModel()
                {
                    Services = _context.Service.Where(x => x.IsDeleted != true)
                                                           .OrderBy(x => x.OrderNo)
                                                           .ToList(),
                    Service = _context.Service.First()
                };
            }
           

            Meta meta = _context.Meta.FirstOrDefault(x => x.Id == 18);

            ViewBag.Author = meta.Author;
            ViewBag.Description = meta.Description;
            ViewBag.Keywords = meta.Keywords;
            ViewBag.Title = meta.Title;
            ViewBag.Page = "/Sektörler";
            if (lang == null)
            {
                ViewBag.Dil = "tr";
            }
            else
            {
                ViewBag.Dil = lang;
            }
            return View(model);
        }

        [Route("/iletisim", Name = "İletişim")]
        [Route("/{lang}/iletisim", Name = "İletişimlang")]
        public IActionResult Contact(string lang)
        {
            GeneralViewModel model = new GeneralViewModel()
            {
                Contacts = _context.Contact.Where(x => x.IsDeleted != true)
                                        .OrderBy(x => x.OrderNo)
                                        .ToList(),
                Socials = _context.Social.Where(x => x.IsDeleted != true)
                                         .OrderBy(x => x.OrderNo)
                                         .ToList()
            };

            Meta meta = _context.Meta.FirstOrDefault(x => x.Id == 8);

            ViewBag.Author = meta.Author;
            ViewBag.Description = meta.Description;
            ViewBag.Keywords = meta.Keywords;
            ViewBag.Title = meta.Title;
            ViewBag.Page = "/iletisim";
            if (lang == null)
            {
                ViewBag.Dil = "tr";
            }
            else
            {
                ViewBag.Dil = lang;
            }
            return View(model);
        }

        [Route("/SendBilgi", Name = "SendBilgi")]
        public IActionResult SendBilgi([FromBody]BilgiMail item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Herhangi bir şey girmediniz";
                return NotFound(data);
            }


            try
            {
                string body = "<html><head><title>Dört Yıldız Eğitim Kurumları | Bilgi Formu</title></head><body>" +
                    "<table style='width:100%'><tr>" +
                    "<td style='width:33%'></td>" +
                    "<td style='width:33%'>" +
                    "<table style='width:33%;text-align:left'>" +
                    "<tr><td>Ad, Soyad</td><td>:</td><td>" + item.Name + "</td></tr>" +
                    "<tr>" +
                        "<td>Telefon</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Phone + "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td>Email</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Email + "</td>" +
                    "</tr>" +
                "</table>" +
            "</td>" +
            "<td style='width:33%'></td>" +
        "</tr>" +
   " </table>" +
"</body>" +
"</html>";

                var fromAddress = new MailAddress("info@dortyildizegitimkurumlari.com.tr");
                var toAddress = new MailAddress("bilgi@dortyildizegitimkurumlari.com.tr");
                const string subject = " Dört Yıldız Eğitim Kurumları | Bilgi Formu";
                using (var smtp = new SmtpClient
                {
                    Host = "mail.dortyildizegitimkurumlari.com.tr",
                    Port = 587,
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, "DortYildiz12")
                })
                {
                    using (var messages = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = body })
                    {
                        messages.IsBodyHtml = true;
                        smtp.Send(messages);
                    }
                }

                data.Message = "Mailiniz gönderilmiştir. En kısa sürede tarafınıza bilgi verilecektir.";
                return Ok(data);

            }
            catch (SmtpException smex)
            {
                data.Message = "Bir hata oluştu daha sonra tekrar deneyiniz.";
                return BadRequest(data);
            }

        }

        [Route("/SendMail", Name = "SendMail")]
        public IActionResult SendMail([FromBody]BilgiMail item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Herhangi bir şey girmediniz";
                return NotFound(data);
            }


            try
            {
                string body = "<html><head><title>teknobant | iletişim Formu</title></head><body>" +
                      "<table style='width:100%'><tr>" +
                    "<td style='width:33%'></td>" +
                    "<td style='width:33%'>" +
                    "<table style='width:100%;text-align:left'>" +
                    "<tr><td>Ad, Soyad</td><td>:</td><td>" + item.Name + "</td></tr>" +
                    "<tr>" +
                        "<td>Email</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Email + "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td>Cep</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Phone + "</td>" +
                    "</tr>" +
                     "<tr>" +
                        "<td>Konu</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Subject + "</td>" +
                    "</tr>" +
                     "<tr>" +
                        "<td>Email</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Message + "</td>" +
                    "</tr>" +
                "</table>" +
            "</td>" +
            "<td style='width:33%'></td>" +
        "</tr>" +
   " </table>" +
"</body>" +
"</html>";

                var fromAddress = new MailAddress("info@teknobant.com");
                var toAddress = new MailAddress("info@teknobant.com");
                const string subject = " teknobant | İletişim Formu";
                using (var smtp = new SmtpClient
                {
                    Host = "mail.teknobant.com.tr",
                    Port = 587,
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, "EWoj60R9")
                })
                {
                    using (var messages = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = body })
                    {
                        messages.IsBodyHtml = true;
                        smtp.Send(messages);
                    }
                }

                data.Message = "Mailiniz gönderilmiştir. En kısa sürede tarafınıza bilgi verilecektir.";
                return Ok(data);

            }
            catch (SmtpException smex)
            {
                data.Message = "Bir hata oluştu daha sonra tekrar deneyiniz.";
                return BadRequest(data);
            }

        }

        [Route("/SendRegister", Name = "SendRegister")]
        public IActionResult SendRegister([FromBody]BilgiMail item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Herhangi bir şey girmediniz";
                return NotFound(data);
            }


            try
            {
                string body = "<html><head><title>Dört Yıldız Eğitim Kurumları | Bilgi Formu</title></head><body>" +
                    "<table style='width:100%'><tr>" +
                    "<td style='width:33%'></td>" +
                    "<td style='width:33%'>" +
                    "<table style='width:100%;text-align:left'>" +
                    "<tr><td>Öğrenci Ad, Soyad</td><td>:</td><td>" + item.Name + "</td></tr>" +
                    "<tr>" +
                        "<td>Doğum Tarihi</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Birthdate + "</td>" +
                    "</tr>" +
                     "<tr>" +
                        "<td>Gideceği Sınıf</td>" +
                        "<td>:</td>" +
                        "<td>" + item.GotoClass + "</td>" +
                    "</tr>" +
                     "<tr>" +
                        "<td>Devam Ettiği Okul</td>" +
                        "<td>:</td>" +
                        "<td>" + item.ContinueSchool + "</td>" +
                    "</tr>" +
                     "<tr>" +
                        "<td>Veli Ad, Soyad</td>" +
                        "<td>:</td>" +
                        "<td>" + item.VeliName + "</td>" +
                    "</tr>" +
                    "</tr>" +
                     "<tr>" +
                        "<td>Telefon</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Phone + "</td>" +
                    "</tr>" +
                     "<tr>" +
                        "<td>Email</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Email + "</td>" +
                    "</tr>" +
                     "<tr>" +
                        "<td>Adres</td>" +
                        "<td>:</td>" +
                        "<td>" + item.Message + "</td>" +
                    "</tr>" +
                "</table>" +
            "</td>" +
            "<td style='width:33%'></td>" +
        "</tr>" +
   " </table>" +
"</body>" +
"</html>";

                var fromAddress = new MailAddress("info@dortyildizegitimkurumlari.com.tr");
                var toAddress = new MailAddress("kayit@dortyildizegitimkurumlari.com.tr");
                const string subject = " Dört Yıldız Eğitim Kurumları | Ön Kayıt Formu";
                using (var smtp = new SmtpClient
                {
                    Host = "mail.dortyildizegitimkurumlari.com.tr",
                    Port = 587,
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, "DortYildiz12")
                })
                {
                    using (var messages = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = body })
                    {
                        messages.IsBodyHtml = true;
                        smtp.Send(messages);
                    }
                }

                data.Message = "Mailiniz gönderilmiştir. En kısa sürede tarafınıza bilgi verilecektir.";
                return Ok(data);

            }
            catch (SmtpException smex)
            {
                data.Message = "Bir hata oluştu daha sonra tekrar deneyiniz.";
                return BadRequest(data);
            }

        }
    }
}