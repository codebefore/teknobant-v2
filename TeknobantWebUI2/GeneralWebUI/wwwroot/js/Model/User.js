var app = angular.module('UserApp', []);
app.controller('UserCtrl', function ($scope, $http) {
    $("#ayarlar").addClass("open");
    $("#ayarlar").addClass("active");
    $("#user").addClass("active");
    $scope.looder = true;
    var index = 0;
    getUsers();
    function getUsers() {
        $http({
            method: "GET",
            url: "/Admin/GetUsers"
        }).then(function success(response) {
            $scope.model = response.data.model;
            $scope.looder = false;
        },
            function error(reason) {
                $scope.looder = false;
                $scope.error = reason.data.message;
                alertify.error(reason.data.message);

            });
    }
    function getSelectIndex(id) {
        for (var i = 0; i < $scope.model.length; i++) {
            if ($scope.model[i].id === id) {
                return i;
            }
        }
    }
    function clear() {
        $scope.name = undefined;
        $scope.email = undefined;
        $scope.password = undefined;
    }

    $scope.openAdd = function () {
        $("#addModal").modal("show");
    };
    $scope.add = function () {

        var item = {
            name: $scope.name,
            email: $scope.email,
            password: $scope.password
        };

        console.log(item);
        $http({
            method: "POST",
            url: "/Admin/AddUser/",
            data: JSON.stringify(item)
        }).then(function success(response) {
            $scope.model.push(response.data.model);
            clear();
            $("#addModal").modal("hide");
            alertify.success(response.data.message);


        }, function unsuccess(reason) {
            alertify
                .delay(5000)
                .error(reason.data);
        });

    };
    $scope.update = function () {

        index = getSelectIndex($scope.id);
        $scope.model[index].name = $scope.name;
        $scope.model[index].email = $scope.email;
        $scope.model[index].password = $scope.password;
        $http({
            method: "POST",
            url: "/Admin/UpdateUser",
            data: $scope.model[index]
        }).then(function success(response) {
            alertify.success(response.data.message);
            $("#editModal").modal("hide");
            clear();

        }, function unsuccess(response) {
            alertify.error(response.data.message);
        });


    };
    $scope.edit = function (id) {
        $("#editModal").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.name = $scope.model[index].name;
        $scope.email = $scope.model[index].email;
        $scope.password = $scope.model[index].password;
    };
    $scope.isDelete = function (id) {
        $("#ModalDelete").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.name = $scope.model[index].name;
        $scope.item = $scope.model[index];
    };
    $scope.delete = function () {

        $http({
            method: "POST",
            url: "/Admin/DeleteUser",
            data: $scope.item
        }).then(function success(response) {
            index = getSelectIndex($scope.id);
            $scope.model.splice(index, 1);
            alertify.success(response.data.message);
            clear();
        }, function unsuccess(response) {

            alertify.error(response.data.message);
        });


    };
});