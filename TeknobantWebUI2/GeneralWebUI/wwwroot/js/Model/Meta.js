var app = angular.module('MetaApp', []);
app.controller('MetaCtrl', function ($scope, $http) {
    $scope.looder = true;
    $("#ayarlar").addClass("open");
    $("#ayarlar").addClass("active");
    $("#meta").addClass("active");
    var index = 0;
    getMetas();
    function getMetas() {
        $http({
            method: "GET",
            url: "/Admin/GetMetas"
        }).then(function success(response) {
            $scope.model = response.data.model;
            $scope.looder = false;
            alertify.success(response.data.message);
        },
            function error(reason) {
                $scope.looder = false;
                $scope.error = reason.data.message;
                alertify.error(reason.data.message);

            });
    }
    function getSelectIndex(id) {
        for (var i = 0; i < $scope.model.length; i++) {
            if ($scope.model[i].id === id) {
                return i;
            }
        }
    }
    function clear() {
        $scope.title = undefined;
        $scope.keywords = undefined;
        $scope.description = undefined;
    }

    $scope.openAdd = function () {
        $("#addModal").modal("show");
    };
    $scope.add = function () {

        var item = {
            title: $scope.title,
            keywords: $scope.keywords,
            description: $scope.description
        };

        console.log(item);
        $http({
            method: "POST",
            url: "/Admin/AddMeta/",
            data: JSON.stringify(item)
        }).then(function success(response) {
            $scope.model.push(response.data.model);
            clear();
            $("#addModal").modal("hide");
            alertify.success(response.data.message);


        }, function unsuccess(reason) {
            alertify
                .delay(5000)
                .error(reason.data);
        });

    };
    $scope.update = function () {

        index = getSelectIndex($scope.id);
        $scope.model[index].title = $scope.title;
        $scope.model[index].keywords = $scope.keywords;
        $scope.model[index].description = $scope.description;
        $http({
            method: "POST",
            url: "/Admin/UpdateMeta",
            data: $scope.model[index]
        }).then(function success(response) {
            alertify.success(response.data.message);
            $("#editModal").modal("hide");
            clear();

        }, function unsuccess(response) {
            alertify.error(response.data.message);
        });


    };
    $scope.edit = function (id) {
        $("#editModal").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.title = $scope.model[index].title;
        $scope.keywords = $scope.model[index].keywords;
        $scope.description= $scope.model[index].description;
    };
    $scope.isDelete = function (id) {
        $("#ModalDelete").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.title = $scope.model[index].title;
        $scope.item = $scope.model[index];
    };
    $scope.delete = function () {

        $http({
            method: "POST",
            url: "/Admin/DeleteMeta",
            data: $scope.item
        }).then(function success(response) {
            index = getSelectIndex($scope.id);
            $scope.model.splice(index, 1);
            alertify.success(response.data.message);
            clear();
        }, function unsuccess(response) {

            alertify.error(response.data.message);
        });


    };
});

$(function () {
    $("#sortable").sortable({
        stop: function (event, ui) {
            var data = {
                items: []
            };
            $("#sortable li").each(function () {

                data.items.push({
                    "Id": $(this).find(".spanId").html(),
                    "OrderNo": $(this).find(".spanOrderNo").html(),
                });

            });
            //var finalval = JSON.stringify(data);
            //alert(finalval);
            $.ajax({
                type: "POST",
                url: "/Admin/UpdateMetaOrder",
                data: JSON.stringify(data.items),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //window.location.reload(); Sayfa refresh
                    console.log(response.message);
                    alertify.success(response.message);

                },
                error: function (reason) {
                    alertify.error(reason.message);
                }
            });
        }
    });
    $("#sortable").disableSelection();
});