var app = angular.module('ProductApp', ['summernote', 'uiCropper']);
app.directive('ngFiles', ['$parse', function ($parse) {
    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);

        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });


    }

    return {
        link: fn_link
    }
}]);
app.controller('ProductCtrl', function ($scope, $http) {
    $scope.looder = true;
    $scope.imageLooder = false;
    $scope.error = undefined;
    var index = 0;
    function getProducts() {
        $http({
            method: "GET",
            url: "/Admin/GetProducts"
        }).then(function success(response) {
            console.log(response.data.model);
            $scope.model = response.data.model;
            $scope.looder = false;
            alertify.success(response.data.message);
            getProductCategories();

        }, function error(reason) {
            console.log(reason);
            $scope.looder = false;
            $scope.error = reason.data.message;
            alertify.error(reason.data.message);

        });
    }
    function getProductCategories() {
        $http({
            method: "GET",
            url: "/Admin/GetProductCategorys"
        }).then(function success(response) {
            console.log(response.data.model);
            $scope.categories = response.data.model;
            $scope.looder = false;
            alertify.success(response.data.message);
        }, function error(reason) {
            console.log(reason);
            $scope.looder = false;
            $scope.error = reason.data.message;
            alertify.error(reason.data.message);

        });
    }
    getProducts();

   
    function getSelectIndex(id) {
        for (var i = 0; i < $scope.model.length; i++) {
            if ($scope.model[i].id === id) {
                return i;
            }
        }
    }
    function clear() {
        $scope.name = undefined;
        $scope.description = undefined;
        $scope.shortDescription = undefined;
        $scope.nameEn = undefined;
        $scope.descriptionEn = undefined;
        $scope.shortDescriptionEn = undefined;
        $scope.nameGe = undefined;
        $scope.descriptionGe = undefined;
        $scope.shortDescriptionGe = undefined;
    }

    $scope.openAdd = function () {
        $("#addModal").modal({ backdrop: 'static', keyboard: false });
    };
    $scope.add = function () {

        var item = {
            productCategoryId: $scope.productCategoryId,
            name: $scope.name,
            description: $scope.description,
            shortDescription: $scope.shortDescription,
            nameEn: $scope.nameEn,
            descriptionEn: $scope.descriptionEn,
            shortDescriptionEn: $scope.shortDescriptionEn,
            nameGe: $scope.nameGe,
            descriptionGe: $scope.descriptionGe,
            shortDescriptionGe: $scope.shortDescriptionGe
        };

        console.log(item);
        $http({
            method: "POST",
            url: "/Admin/AddProduct/",
            data: JSON.stringify(item)
        }).then(function success(response) {
            $scope.model.push(response.data.model);

            clear();
            $("#addModal").modal("hide");
            alertify.success(response.data.message);


        }, function unsuccess(reason) {
            alertify
                .delay(5000)
                .error(reason.data.message);
        });

    };
    $scope.GetImages = function (id) {
        $scope.productId = id;
        $scope.productIndex = getSelectIndex(id);

        console.log($scope.model[$scope.productIndex].images.length);
        if ($scope.model[$scope.productIndex].images.length > 0) {
            $scope.productImages = $scope.model[$scope.productIndex].images;
        }
        else {
            $scope.productImages = [];
        }
        console.log($scope.productImages);
        $('#addImagesModal').modal("show");
    };
    $scope.addImages = function () {

        $http({
            method: "POST",
            url: "/Admin/AddProductImages/",
            data: $scope.productId
        }).then(function success(response) {
            console.log(response);
            for (var i = 0; i < response.data.model.length; i++) {
                $scope.model[$scope.productIndex].images.push(response.data.model[i]);
            }
            clear();
            //$("#addImagesModal").modal("hide");
            alertify.success(response.data.message);
            $scope.ImgLoaded = false;
            $scope.reset();
        }, function unsuccess(reason) {
            alertify
                .delay(5000)
                .error(reason.data.message);
        });

    };
    $scope.update = function () {

        index = getSelectIndex($scope.id); 
        $scope.model[index].productCategoryId = $scope.productCategoryId;
        $scope.model[index].name = $scope.name;
        $scope.model[index].description = $scope.description;
        $scope.model[index].shortDescription = $scope.shortDescription;
        $scope.model[index].nameEn = $scope.nameEn;
        $scope.model[index].descriptionEn = $scope.descriptionEn;
        $scope.model[index].shortDescriptionEn = $scope.shortDescriptionEn;
        $scope.model[index].nameGe = $scope.nameGe;
        $scope.model[index].descriptionGe = $scope.descriptionGe;
        $scope.model[index].shortDescriptionGe = $scope.shortDescriptionGe;
        $http({
            method: "POST",
            url: "/Admin/UpdateProduct",
            data: $scope.model[index]
        }).then(function success(response) {
            alertify.success(response.data.message);
            $("#editModal").modal("hide");
            clear();

        }, function unsuccess(response) {
            alertify.error(response.data.message);
        });


    };
    $scope.edit = function (id) {
        $("#editModal").modal({ backdrop: 'static', keyboard: false });
        $scope.id = id;
        index = getSelectIndex(id); 
        $scope.productCategoryId = $scope.model[index].productCategoryId.toString();
        $scope.name = $scope.model[index].name;
        $scope.description = $scope.model[index].description;
        $scope.shortDescription = $scope.model[index].shortDescription;
        $scope.nameEn = $scope.model[index].nameEn;
        $scope.descriptionEn = $scope.model[index].descriptionEn;
        $scope.shortDescriptionEn = $scope.model[index].shortDescriptionEn;
        $scope.nameGe = $scope.model[index].nameGe;
        $scope.descriptionGe = $scope.model[index].descriptionGe;
        $scope.shortDescriptionGe = $scope.model[index].shortDescriptionGe;
    };
    $scope.isDelete = function (id) {
        $("#ModalDelete").modal("show");
        $scope.id = id;
        index = getSelectIndex(id);
        $scope.name = $scope.model[index].name;
        $scope.item = $scope.model[index];
    };
    $scope.delete = function () {

        $http({
            method: "POST",
            url: "/Admin/DeleteProduct",
            data: $scope.item
        }).then(function success(response) {
            index = getSelectIndex($scope.id);
            $scope.model.splice(index, 1);
            alertify.success(response.data.message);
            clear();
        }, function unsuccess(response) {

            alertify.error(response.data.message);
        });


    };

    $scope.deleteImage = function (img) {

        $http({
            method: "POST",
            url: "/Admin/DeleteProductImages",
            data: img
        }).then(function success(response) {
            for (var i = 0; i < $scope.model[$scope.productIndex].images.length; i++) {
                if ($scope.model[$scope.productIndex].images[i].id === img.id) {
                    $scope.model[$scope.productIndex].images.splice(i, 1);
                }
            }
            alertify.success(response.data.message);
            clear();
        }, function unsuccess(response) {

            alertify.error(response.data.message);
        });


    };
    $scope.GetPrices = function (id) {
        $scope.productid = id;
        $scope.productIndex = getSelectIndex(id);
        $("#addPriceModal").modal("show");


    };
    $scope.addPrice = function () {
        var item = {
            productId: $scope.productid,
            name: $scope.name,
            price: $scope.price
        };
       

        $http({
            method: "POST",
            url: "/Admin/AddProductPrices/",
            data: JSON.stringify(item)
        }).then(function success(response) {
            $scope.model[$scope.productIndex].prices.push(response.data.model);
            console.log($scope.model[$scope.productIndex]);
            clear();
            $("#addModal").modal("hide");
            alertify.success(response.data.message);

        }, function unsuccess(reason) {
            alertify
                .delay(5000)
                .error(reason.data.message);
        });

    };
    
    $scope.deletePrice = function (item) {
        

        $http({
            method: "POST",
            url: "/Admin/DeleteProductPrices",
            data: item
        }).then(function success(response) {
            for (var i = 0; i < $scope.model[$scope.productIndex].prices.length; i++) {
                if ($scope.model[$scope.productIndex].prices[i].id === item.id) {
                    $scope.model[$scope.productIndex].prices.splice(i, 1);
                }
            }
            alertify.success(response.data.message);
            clear();
        }, function unsuccess(response) {

            alertify.error(response.data.message);
        });


    };

    var formdata = new FormData();
    $scope.getTheFiles = function ($files) {
        $scope.imagesrc = [];
        for (var i = 0; i < $files.length; i++) {
            var reader = new FileReader();
            reader.filename = $files[i].name;
            reader.onload = function (event) {
                var image = {};
                image.name = event.target.name;
                //image.size = (event.total / 1024).toFixed(2);
                image.Src = event.target.result;
                $scope.imagesrc.push(image);
                $scope.FileName = image.Src;
                $scope.$apply();
            };

            reader.readAsDataURL($files[i]);
        }
        angular.forEach($files, function (value, key) {
            formdata.append(key, value);

        });
    };
    $scope.isUpload = false;
    $scope.uploadFiles = function () {
        $scope.imageLooderMessage = "Lutfen bekleyiniz";
        $scope.imageLooder = true;
        $scope.ImgLoaded = false;
        $scope.ImgLooding = true;
        alertify.delay(9000).log('Yukleniyor...');

        $http({
            method: "POST",
            url: "/Admin/UploadFile",
            data: JSON.stringify($scope.myCroppedImage)
        }).then(function success(response) {
            alertify
                .delay(5000)
                .success(response.data.message);
            $scope.isUpload = true;
            $scope.isAdd = true;

            $scope.reset();
            $scope.ImgLoaded = true;
            $scope.ImgLooding = false;
            $scope.imageLooder = false;
            $scope.imageLooderMessage = undefined;


        }, function error(reason) {
            $scope.ImgLooding = false;
            $scope.imageLooder = false;

            console.log(reason);
            alertify
                .delay(5000)
                .error(reason.data.message);
            $scope.reset();
        });


    };
    $scope.reset = function () {
        angular.forEach(
            angular.element["input [type = 'file']"], function (inputElem) {
                angular.element(inputElem).val(null);
            }
        );
        $scope.imagesrc = [];
        formdata = new FormData();
        $scope.myImage = '';
        $scope.myCroppedImage = '';
    };
    $scope.myImage = '';
    $scope.myCroppedImage = '';
    var handleFileSelect = function (evt) {
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function ($scope) {
                $scope.myImage = evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
});

$("#products").addClass("active");
$(function () {
    $("#sortable").sortable({
        stop: function (event, ui) {
            var data = {
                items: []
            };
            $("#sortable li").each(function () {

                data.items.push({
                    "Id": $(this).find(".spanId").html(),
                    "OrderNo": $(this).find(".spanOrderNo").html()
                });

            });
            //var finalval = JSON.stringify(data);
            //alert(finalval);
            $.ajax({
                type: "POST",
                url: "/Admin/UpdateProductOrder",
                data: JSON.stringify(data.items),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //window.location.reload(); Sayfa refresh
                    console.log(response.message);
                    alertify.success(response.message);

                },
                error: function (reason) {
                    alertify.error(reason.message);
                }
            });
        }
    });
    $("#sortable").disableSelection();
});
$(function () {
    $("#sortableImages").sortable({
        stop: function (event, ui) {
            var data = {
                items: []
            };
            $("#sortableImages li").each(function () {

                data.items.push({
                    "Id": $(this).find(".spanImageId").html(),
                    "OrderNo": $(this).find(".spanImageOrderNo").html(),
                    "ProductId": $(this).find(".spanProductId").html()
                });

            });
            //var finalval = JSON.stringify(data);
            //alert(finalval);
            $.ajax({
                type: "POST",
                url: "/Admin/UpdateProductImagesOrder",
                data: JSON.stringify(data.items),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //window.location.reload(); Sayfa refresh
                    console.log(response.message);
                    alertify.success(response.message);

                },
                error: function (reason) {
                    alertify.error(reason.message);
                }
            });
        }
    });
    $("#sortable").disableSelection();
});