﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class Duyuru:BaseEntity
    {
        
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
