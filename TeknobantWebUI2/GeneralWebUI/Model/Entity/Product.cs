﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class Product:BaseEntity
    {
        public int ProductCategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string NameEn { get; set; }
        public string DescriptionEn { get; set; }
        public string ShortDescriptionEn { get; set; }
        public string NameGe { get; set; }
        public string DescriptionGe { get; set; }
        public string ShortDescriptionGe { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public List<ProductImage> Images { get; set; }
    }
}
