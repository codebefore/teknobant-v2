﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class Visitor:BaseEntity
    {

        public Visitor()
        {
            Date = DateTime.Now;
        }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
        public bool Confirmation { get; set; } = false;
        public DateTime Date { get; set; }
        
    }
}
