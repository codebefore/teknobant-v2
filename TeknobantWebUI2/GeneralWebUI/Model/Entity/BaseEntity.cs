﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public int OrderNo { get; set; } = 0;
        public bool IsDeleted { get; set; } = false;
    }
}
