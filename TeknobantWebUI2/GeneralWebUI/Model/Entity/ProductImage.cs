﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class ProductImage : BaseEntity
    {

        public int ProductId { get; set; }
        public string LocalUrl { get; set; }
        public string CloudUrl { get; set; }
        public string PublicId { get; set; }
        public string Alt { get; set; }
        public bool IsMain { get; set; }

        public Product Product { get; set; }

    }
}
