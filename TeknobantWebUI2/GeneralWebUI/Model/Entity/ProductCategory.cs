﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model
{
    public class ProductCategory : BaseEntity
    {
        
        public string Name { get; set; }
        public string NameEn { get; set; }
        public string NameGe { get; set; }
        public List<Product> Products { get; set; }
    }
}
