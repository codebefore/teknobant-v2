﻿using GeneralWebUI.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model.ViewModel
{
    public class ProductViewModel
    {
        public List<Product> Products { get; set; }
        public Product Product { get; set; }
        public Product PreviousProduct { get; set; }
        public Product NextProduct { get; set; }

    }
}
