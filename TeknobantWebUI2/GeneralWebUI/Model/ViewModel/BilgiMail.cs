﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model.ViewModel
{
    public class BilgiMail
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }

        public string VeliName { get; set; }
        public string Birthdate { get; set; }
        public string GotoClass { get; set; }
        public string ContinueSchool { get; set; }

    }
}
