﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneralWebUI.Model.ViewModel
{
    public class ImageProperty
    {
        public string CloudinaryUrl { get; set; }
        public string ImageUrl { get; set; }
        public string PublicId { get; set; }
    }
}