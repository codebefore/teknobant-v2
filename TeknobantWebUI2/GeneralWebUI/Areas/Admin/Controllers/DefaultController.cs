﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using GeneralWebUI.Helper;
using GeneralWebUI.Model;
using GeneralWebUI.Model.Entity;
using GeneralWebUI.Model.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TinifyAPI;

namespace GeneralWebUI.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class DefaultController : Controller
    {
        readonly LoginContext _context;
        readonly IOptions<CloudinarySettings> _cloudinarySettings;
        readonly IOptions<TinifySettings> _tinifySettings;
        readonly Cloudinary _cloudinary;
        readonly IHostingEnvironment _hostingEnvironment;
        public DefaultController(LoginContext context, IOptions<CloudinarySettings> cloudinarySettings, IHostingEnvironment hostingEnvironment, IOptions<TinifySettings> tinifySettings)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
            _cloudinarySettings = cloudinarySettings;
            _tinifySettings = tinifySettings;
            Account account = new Account
            {
                ApiKey = _cloudinarySettings.Value.ApiKey,
                ApiSecret = _cloudinarySettings.Value.ApiSecret,
                Cloud = _cloudinarySettings.Value.CloudName
            };
            _cloudinary = new Cloudinary(account);

        }

        public void SetCookie(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddHours(expireTime.Value);
            else
            {
                option.Expires = DateTime.Now.AddHours(2);
            }

            Response.Cookies.Append(key, value, option);

        }
        public string GetCookie(string key)
        {
            return Request.Cookies[key];
        }

        #region Login
        [Route("Login", Name = "Login")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("Admin/LoginControl", Name = "LoginControl")]
        public JsonResult LoginControl([FromBody]Login item)
        {
            Data data = new Data();
            Login login = _context.Login.FirstOrDefault(x => x.Email == item.Email &&
                                                             x.Password == item.Password &&
                                                             x.IsDeleted != true);
            if (login != null)
            {
                SetCookie("user", login.Id.ToString(), null);
                data.Message = "Başarılı";
                data.Success = true;
                return Json(data);

            }

            data.Message = "Kullanıcı Bulunamadı.";
            data.Success = false;
            return Json(data);
        }
        #endregion
        #region User
        [Route("Admin/Users", Name = "Users")]
        public IActionResult Users()
        {
            var asd = Request.Cookies["user"];
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetUsers", Name = "GetUsers")]
        public IActionResult GetUsers()
        {
            Data data = new Data
            {
                Model = _context.Login.Where(x => x.IsDeleted != true).ToList()
            };

            return Ok(data);
        }

        [HttpPost("Admin/AddUser", Name = "AddUser")]
        public IActionResult AddUser([FromBody]Login item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }
            item.OrderNo = 0;
            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteUser", Name = "DeleteUser")]
        public IActionResult DeleteUser([FromBody]Login item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateUser", Name = "UpdateUser")]
        public IActionResult UpdateUser([FromBody]Login item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }
        #endregion
        #region Menu
        [Route("Admin/Menus", Name = "Menus")]
        public IActionResult Menus()
        {
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetMenus", Name = "GetMenus")]
        public IActionResult GetMenus()
        {
            Data data = new Data
            {
                Model = _context.Menu.Where(x => x.IsDeleted != true)
                                     .OrderBy(x => x.OrderNo)
                                     .ToList(),
            };


            if (data.Model != null)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }


            data.Message = "Veriler yüklenemedi.";
            return NotFound();


        }

        [HttpPost("Admin/AddMenu", Name = "AddMenu")]
        public IActionResult AddMenu([FromBody]Menu item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }

            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteMenu", Name = "DeleteMenu")]
        public IActionResult DeleteMenu([FromBody]Menu item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateMenu", Name = "UpdateMenu")]
        public IActionResult UpdateMenu([FromBody]Menu item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateMenuOrder", Name = "UpdateMenuOrder")]
        public IActionResult UpdateMenuOrder([FromBody]List<Menu> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Menu> itemlist = _context.Menu.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Menu item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion

        #region ProductCategory
        [Route("Admin/ProductCategorys", Name = "ProductCategorys")]
        public IActionResult ProductCategorys()
        {
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetProductCategorys", Name = "GetProductCategorys")]
        public IActionResult GetProductCategorys()
        {
            Data data = new Data
            {
                Model = _context.ProductCategory.Where(x => x.IsDeleted != true)
                                     .OrderBy(x => x.OrderNo)
                                     .ToList(),
            };


            if (data.Model != null)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }


            data.Message = "Veriler yüklenemedi.";
            return NotFound();


        }

        [HttpPost("Admin/AddProductCategory", Name = "AddProductCategory")]
        public IActionResult AddProductCategory([FromBody]ProductCategory item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }

            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteProductCategory", Name = "DeleteProductCategory")]
        public IActionResult DeleteProductCategory([FromBody]ProductCategory item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateProductCategory", Name = "UpdateProductCategory")]
        public IActionResult UpdateProductCategory([FromBody]ProductCategory item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateProductCategoryOrder", Name = "UpdateProductCategoryOrder")]
        public IActionResult UpdateProductCategoryOrder([FromBody]List<ProductCategory> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<ProductCategory> itemlist = _context.ProductCategory.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    ProductCategory item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion
        #region Slider
        [Route("Admin/Sliders", Name = "Sliders")]
        public IActionResult Sliders()
        {


            //if (string.IsNullOrEmpty(Request.Cookies["user"]))
            //{
            //    return RedirectToRoute("Login");
            //}

            return View();
        }

        [HttpGet("Admin/GetSliders", Name = "GetSliders")]
        public IActionResult GetSliders()
        {
            Data data = new Data
            {
                Model = _context.Slider.Where(x => x.IsDeleted != true)
                                       .OrderBy(x => x.OrderNo)
                                       .ToList()
            };

            return Ok(data);
        }

        [HttpPost("Admin/AddSlider", Name = "AddSlider")]
        public IActionResult AddSlider([FromBody]Slider item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }

            if (IsUploadImage)
            {
                item.PublicId = ImgList.First().PublicId;
                item.LocalUrl = ImgList.First().ImageUrl;
                item.CloudUrl = ImgList.First().CloudinaryUrl;
                item.OrderNo = 0;
                var addedEntity = _context.Entry(item);
                addedEntity.State = EntityState.Added;
                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Başarılı";
                    data.Model = item;
                    return Ok(data);

                }
            }

            ImgList.Clear();
            IsUploadImage = false;
            data.Message = "Resim Yüklemediniz";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteSlider", Name = "DeleteSlider")]
        public IActionResult DeleteSlider([FromBody]Slider item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateSlider", Name = "UpdateSlider")]
        public IActionResult UpdateSlider([FromBody]Slider item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateSliderOrder", Name = "UpdateSliderOrder")]
        public IActionResult UpdateSliderOrder([FromBody]List<Slider> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Slider> itemlist = _context.Slider.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Slider item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion


        #region UploadImage
        [Route("Admin/UploadImage", Name = "UploadImage")]
        public IActionResult UploadImage()
        {
            return View();
        }

        [HttpGet("Admin/GetUploadImages", Name = "GetUploadImages")]
        public IActionResult GetUploadImages()
        {
            Data data = new Data
            {
                Model = _context.UploadImage.Where(x => x.IsDeleted != true)
                                       .OrderBy(x => x.OrderNo)
                                       .ToList()
            };

            data.Message = "Yükleme Başarılı";
            return Ok(data);
        }

        [HttpPost("Admin/AddUploadImage", Name = "AddUploadImage")]
        public IActionResult AddUploadImage([FromBody]UploadImage item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }

            if (IsUploadImage)
            {
                item.PublicId = ImgList.First().PublicId;
                item.LocalUrl = ImgList.First().ImageUrl;
                item.CloudUrl = ImgList.First().CloudinaryUrl;
                item.OrderNo = 0;
                var addedEntity = _context.Entry(item);
                addedEntity.State = EntityState.Added;
                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Başarılı";
                    data.Model = item;
                    return Ok(data);

                }
            }

            ImgList.Clear();
            IsUploadImage = false;
            data.Message = "UploadImage Yüklemediniz";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteUploadImage", Name = "DeleteUploadImage")]
        public IActionResult DeleteUploadImage([FromBody]UploadImage item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateUploadImage", Name = "UpdateUploadImage")]
        public IActionResult UpdateUploadImage([FromBody]UploadImage item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateUploadImageOrder", Name = "UpdateUploadImageOrder")]
        public IActionResult UpdateUploadImageOrder([FromBody]List<UploadImage> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<UploadImage> itemlist = _context.UploadImage.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    UploadImage item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion

        public static List<ImageProperty> ImgList { get; set; }


        public static bool IsUploadImage { get; set; }

        public async Task<string> UploadImageCloudinary(FileStream file, string name)
        {
            var uploadResult = new ImageUploadResult();
            if (file.Length > 0)
            {

                var uploadParams = new ImageUploadParams
                {
                    File = new FileDescription(name, file),

                };

                uploadResult = await _cloudinary.UploadAsync(uploadParams);

            }


            return uploadResult.Uri.ToString() + "&" + uploadResult.PublicId;
        }
        public bool TaskTinify(string path, string modifiedpath)
        {

            Tinify.Key = _tinifySettings.Value.Key;
            Tinify.FromFile(path).ToFile(modifiedpath).Wait();
            return true;
        }

        [HttpPost("Admin/UploadFiles", Name = "UploadFiles")]
        public async Task<IActionResult> UploadFiles()
        {
            Data data = new Data();
            IsUploadImage = true;
            ImgList = new List<ImageProperty>();

            var files = HttpContext.Request.Form.Files;

            if (files.Count > 0)
            {
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {

                        ImageProperty img = new ImageProperty();

                        var file = Image;
                        var path = Path.Combine(_hostingEnvironment.WebRootPath, "images\\uploads");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                            {
                                //Create wwwroot
                                await file.CopyToAsync(fileStream);
                            }

                            string tinifyFileName = "\\" + "tinify" + fileName;
                            string ex = file.FileName.Substring(file.FileName.Length - 3, 3);
                            img.ImageUrl = "/images/uploads/" + fileName;
                            if (ex != "pdf")
                            {
                                try
                                {
                                    //TinyPng
                                    TaskTinify(path + "\\" + fileName, img.ImageUrl);
                                }
                                catch (System.Exception)
                                {

                                }
                                //Cloudinary
                                //string result = await UploadImageCloudinary(file, tinifyFileName);
                                //string[] resultArray = result.Split("&");
                                //img.CloudinaryUrl = resultArray[0];
                                //img.PublicId = resultArray[1];
                                //System.IO.File.Delete(Path.Combine(path, fileName));
                            }
                            ImgList.Add(img);


                        }
                    }
                }

                data.Message = "Resim Yükleme Başarılı";
                return Ok(data);
            }
            data.Message = "Lütfen bir resim seçiniz";
            return NotFound(data);

        }

        [HttpPost("Admin/UploadFile", Name = "UploadFile")]
        public async Task<IActionResult> UploadFile([FromBody]string base64)
        {

            try
            {
                Data data = new Data();
                if (base64 == "")
                {
                    data.Message = "Herhangi bir seçim yapmadınız.";
                    return NotFound(data);
                }
                IsUploadImage = true;
                ImgList = new List<ImageProperty>();

                ImageProperty img = new ImageProperty();
                var path = Path.Combine(_hostingEnvironment.WebRootPath, "images\\uploads");
                string convert = base64.Replace("data:image/png;base64,", String.Empty);
                var base64array = Convert.FromBase64String(convert);
                var fileName = Guid.NewGuid().ToString().Replace("-", "") + ".png";
                System.IO.File.WriteAllBytes(path + "\\" + fileName, base64array);


                string tinifyFileName = "\\" + "tinify" + fileName;
                //TinyPng
                FileStream file;
                try
                {
                    TaskTinify(path + "\\" + fileName, path + tinifyFileName);
                    img.ImageUrl = "/images/uploads/" + tinifyFileName;
                    file = new FileStream(path + tinifyFileName, FileMode.Open);


                }
                catch (System.Exception ex)
                {
                    img.ImageUrl = "/images/uploads/" + fileName;
                    file = new FileStream(path + "\\" + fileName + tinifyFileName, FileMode.Open);

                }

                //Cloudinary
                string result = await UploadImageCloudinary(file, tinifyFileName);
                string[] resultArray = result.Split("&");
                img.CloudinaryUrl = resultArray[0];
                img.PublicId = resultArray[1];
                System.IO.File.Delete(Path.Combine(path, fileName));

                ImgList.Add(img);

                data.Message = "Resim Yükleme Başarılı";
                return Ok(data);

            }
            catch (System.Exception ex)
            {
                return NotFound();
            }
        }

        #region Content
        [Route("Admin/Contents", Name = "Contents")]
        public IActionResult Contents()
        {
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetContents", Name = "GetContents")]
        public IActionResult GetContents()
        {
            Data data = new Data
            {
                Model = _context.Content.Where(x => x.IsDeleted != true)
                                     .OrderBy(x => x.OrderNo)
                                     .ToList(),
            };


            if (data.Model != null)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }


            data.Message = "Veriler yüklenemedi.";
            return NotFound();


        }

        [HttpPost("Admin/AddContent", Name = "AddContent")]
        public IActionResult AddContent([FromBody]Content item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }

            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteContent", Name = "DeleteContent")]
        public IActionResult DeleteContent([FromBody]Content item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateContent", Name = "UpdateContent")]
        public IActionResult UpdateContent([FromBody]Content item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateContentOrder", Name = "UpdateContentOrder")]
        public IActionResult UpdateContentOrder([FromBody]List<Content> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Content> itemlist = _context.Content.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Content item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }


                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion
       
        #region Product
        [Route("Admin/Products", Name = "Products")]
        public IActionResult Products()
        {
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetProducts", Name = "GetProducts")]
        public IActionResult GetProducts()
        {
            List<Product> products = _context.Product.Where(x => x.IsDeleted != true)
                                        .Include(x => x.Images)
                                        .Include(x => x.ProductCategory)
                                        .OrderBy(x => x.OrderNo)
                                        .AsNoTracking()
                                        .ToList();
           
            Data data = new Data
            {
                Model = products,
            };


            if (data.Model != null)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }


            data.Message = "Veriler yüklenemedi.";
            return NotFound(data);


        }

        [HttpPost("Admin/AddProduct", Name = "AddProduct")]
        public IActionResult AddProduct([FromBody]Product item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }

            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteProduct", Name = "DeleteProduct")]
        public IActionResult DeleteProduct([FromBody]Product item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateProduct", Name = "UpdateProduct")]
        public IActionResult UpdateProduct([FromBody]Product item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateProductOrder", Name = "UpdateProductOrder")]
        public IActionResult UpdateProductOrder([FromBody]List<Product> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Product> itemlist = _context.Product.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Product item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }

        //Product Images
        [HttpPost("Admin/AddProductImages", Name = "AddProductImages")]
        public IActionResult AddProductImages([FromBody]int id)
        {
            Data data = new Data();

            if (id == 0)
            {
                data.Message = "Bir hata oluştu";
                return BadRequest(data);
            }

            Product product = _context.Product.SingleOrDefault(x => x.Id == id);

            if (product == null)
            {
                data.Message = "Böyle bir ürün yok";
                return BadRequest(data);

            }
            List<ProductImage> productImages = new List<ProductImage>();
            foreach (var item in ImgList)
            {
                ProductImage img = new ProductImage()
                {
                    CloudUrl = item.CloudinaryUrl,
                    IsDeleted = false,
                    OrderNo = 0,
                    IsMain = false,
                    LocalUrl = item.ImageUrl,
                    PublicId = item.PublicId,
                    ProductId = id,
                };

                var addedEntity = _context.Entry(img);
                addedEntity.State = EntityState.Added;

                productImages.Add(img);
            }



            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = productImages;
                return Ok(data);
            }


            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateProductImagesOrder", Name = "UpdateProductImagesOrder")]
        public IActionResult UpdateProductImagesOrder([FromBody]List<ProductImage> items)
        {

            Data data = new Data();
            if (items != null)
            {
                List<ProductImage> itemlist = _context.ProductImage.Where(x => x.IsDeleted != true && x.ProductId == items.First().ProductId).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    ProductImage item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }

        [HttpPost("Admin/DeleteProductImages", Name = "DeleteProductImages")]
        public IActionResult DeleteProductImages([FromBody]ProductImage item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            string publicid = item.PublicId;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Deleted;
            if (_context.SaveChanges() > 0)
            {
                var delParams = new DelResParams()
                {
                    PublicIds = new List<string>() { publicid },
                    Invalidate = true
                };
                var delResult = _cloudinary.DeleteResourcesAsync(delParams);

                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }
        #endregion

        #region Galeri
        [Route("Admin/References", Name = "References")]
        public IActionResult References()
        {


            if (string.IsNullOrEmpty(Request.Cookies["user"]))
            {
                return RedirectToRoute("Login");
            }

            return View();
        }

        [HttpGet("Admin/GetReferences", Name = "GetReferences")]
        public IActionResult GetReferences()
        {
            Data data = new Data
            {
                Model = _context.Reference.Where(x => x.IsDeleted != true)
                                       .OrderBy(x => x.OrderNo)
                                       .ToList()
            };

            data.Message = "Yükleme Başarılı";
            return Ok(data);
        }

        [HttpPost("Admin/AddReference", Name = "AddReference")]
        public IActionResult AddReference([FromBody]Reference item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }

            if (IsUploadImage)
            {
                item.PublicId = ImgList.First().PublicId;
                item.LocalUrl = ImgList.First().ImageUrl;
                item.CloudUrl = ImgList.First().CloudinaryUrl;
                item.OrderNo = 0;
                var addedEntity = _context.Entry(item);
                addedEntity.State = EntityState.Added;
                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Başarılı";
                    data.Model = item;
                    return Ok(data);

                }
            }

            ImgList.Clear();
            IsUploadImage = false;
            data.Message = "Resim Yüklemediniz";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteReference", Name = "DeleteReference")]
        public IActionResult DeleteReference([FromBody]Reference item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateReference", Name = "UpdateReference")]
        public IActionResult UpdateReference([FromBody]Reference item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateReferenceOrder", Name = "UpdateReferenceOrder")]
        public IActionResult UpdateReferenceOrder([FromBody]List<Reference> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Reference> itemlist = _context.Reference.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Reference item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion

      
        #region Contact
        [Route("Admin/Contacts", Name = "Contacts")]
        public IActionResult Contacts()
        {
            if (string.IsNullOrEmpty(Request.Cookies["user"]))
            {
                return RedirectToRoute("Login");
            }

            return View();
        }

        [HttpGet("Admin/GetContacts", Name = "GetContacts")]
        public IActionResult GetContacts()
        {
            Data data = new Data
            {
                Model = _context.Contact.Where(x => x.IsDeleted != true)
                                       .OrderBy(x => x.OrderNo)
                                       .ToList()
            };

            data.Message = "Yükleme Başarılı";
            return Ok(data);
        }

        [HttpPost("Admin/AddContact", Name = "AddContact")]
        public IActionResult AddContact([FromBody]Contact item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }


            item.OrderNo = 0;
            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);

            }



            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteContact", Name = "DeleteContact")]
        public IActionResult DeleteContact([FromBody]Contact item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateContact", Name = "UpdateContact")]
        public IActionResult UpdateContact([FromBody]Contact item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateContactOrder", Name = "UpdateContactOrder")]
        public IActionResult UpdateContactOrder([FromBody]List<Contact> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Contact> itemlist = _context.Contact.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Contact item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion
        #region Sosyal Medya Linkleri
        [Route("Admin/Socials", Name = "Socials")]
        public IActionResult Socials()
        {
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetSocials", Name = "GetSocials")]
        public IActionResult GetSocials()
        {
            Data data = new Data
            {
                Model = _context.Social.Where(x => x.IsDeleted != true)
                                     .OrderBy(x => x.OrderNo)
                                     .ToList(),
            };


            if (data.Model != null)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }


            data.Message = "Veriler yüklenemedi.";
            return NotFound();


        }

        [HttpPost("Admin/AddSocial", Name = "AddSocial")]
        public IActionResult AddSocial([FromBody]Social item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }

            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteSocial", Name = "DeleteSocial")]
        public IActionResult DeleteSocial([FromBody]Social item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateSocial", Name = "UpdateSocial")]
        public IActionResult UpdateSocial([FromBody]Social item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateSocialOrder", Name = "UpdateSocialOrder")]
        public IActionResult UpdateSocialOrder([FromBody]List<Social> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Social> itemlist = _context.Social.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Social item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion
        #region Meta
        [Route("Admin/Metas", Name = "Metas")]
        public IActionResult Metas()
        {
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetMetas", Name = "GetMetas")]
        public IActionResult GetMetas()
        {
            Data data = new Data
            {
                Model = _context.Meta.Where(x => x.IsDeleted != true)
                                     .OrderBy(x => x.OrderNo)
                                     .ToList(),
            };


            if (data.Model != null)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }


            data.Message = "Veriler yüklenemedi.";
            return NotFound();


        }

        [HttpPost("Admin/AddMeta", Name = "AddMeta")]
        public IActionResult AddMeta([FromBody]Meta item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }
            item.Author = "Seyfettin ÇAM";
            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteMeta", Name = "DeleteMeta")]
        public IActionResult DeleteMeta([FromBody]Meta item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateMeta", Name = "UpdateMeta")]
        public IActionResult UpdateMeta([FromBody]Meta item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateMetaOrder", Name = "UpdateMetaOrder")]
        public IActionResult UpdateMetaOrder([FromBody]List<Meta> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Meta> itemlist = _context.Meta.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Meta item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion
        #region Counter
        [Route("Admin/Counters", Name = "Counters")]
        public IActionResult Counters()
        {
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetCounters", Name = "GetCounters")]
        public IActionResult GetCounters()
        {
            Data data = new Data
            {
                Model = _context.Counter.Where(x => x.IsDeleted != true)
                                     .OrderBy(x => x.OrderNo)
                                     .ToList(),
            };


            if (data.Model != null)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }


            data.Message = "Veriler yüklenemedi.";
            return NotFound();


        }

        [HttpPost("Admin/AddCounter", Name = "AddCounter")]
        public IActionResult AddCounter([FromBody]Counter item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }
            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteCounter", Name = "DeleteCounter")]
        public IActionResult DeleteCounter([FromBody]Counter item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateCounter", Name = "UpdateCounter")]
        public IActionResult UpdateCounter([FromBody]Counter item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateCounterOrder", Name = "UpdateCounterOrder")]
        public IActionResult UpdateCounterOrder([FromBody]List<Counter> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Counter> itemlist = _context.Counter.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Counter item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion
        #region Hizmetler
        [Route("Admin/Services", Name = "Services")]
        public IActionResult Services()
        {
            if (string.IsNullOrEmpty(GetCookie("user")))
            {
                return RedirectToRoute("Login");
            }
            return View();
        }

        [HttpGet("Admin/GetServices", Name = "GetServices")]
        public IActionResult GetServices()
        {
            Data data = new Data
            {
                Model = _context.Service.Where(x => x.IsDeleted != true)
                                     .OrderBy(x => x.OrderNo)
                                     .ToList(),
            };


            if (data.Model != null)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }


            data.Message = "Veriler yüklenemedi.";
            return NotFound();


        }

        [HttpPost("Admin/AddService", Name = "AddService")]
        public IActionResult AddService([FromBody]Service item)
        {
            Data data = new Data();
            if (item == null)
            {
                data.Message = "Lütfen boş alanları doldurunuz";
                return BadRequest(data);
            }
            var addedEntity = _context.Entry(item);
            addedEntity.State = EntityState.Added;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                data.Model = item;
                return Ok(data);
            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/DeleteService", Name = "DeleteService")]
        public IActionResult DeleteService([FromBody]Service item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı.";
                return NotFound(data);
            }
            item.IsDeleted = true;
            var deletedEntity = _context.Entry(item);
            deletedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı.";
                return Ok(data);
            }
            data.Message = "Bir hata oluştu!";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateService", Name = "UpdateService")]
        public IActionResult UpdateService([FromBody]Service item)
        {
            Data data = new Data();

            if (item == null)
            {
                data.Message = "Bulunamadı";
                return NotFound(data);
            }
            var updatedEntity = _context.Entry(item);
            updatedEntity.State = EntityState.Modified;
            if (_context.SaveChanges() > 0)
            {
                data.Message = "Başarılı";
                return Ok(data);

            }
            data.Message = "Bir hata oluştu";
            return BadRequest(data);

        }

        [HttpPost("Admin/UpdateServiceOrder", Name = "UpdateServiceOrder")]
        public IActionResult UpdateServiceOrder([FromBody]List<Service> items)
        {
            Data data = new Data();
            if (items != null)
            {
                List<Service> itemlist = _context.Service.Where(x => x.IsDeleted != true).ToList();

                for (int i = 0; i < items.Count; i++)
                {
                    Service item = itemlist.SingleOrDefault(x => x.Id == items[i].Id);
                    item.OrderNo = i;
                }

                if (_context.SaveChanges() > 0)
                {
                    data.Message = "Sıralama başarılı";
                    return Ok(data);
                }

                data.Message = "Bir hata oluştu";
                return NotFound(data);
            }


            data.Message = "Sıralama başarısız";
            return NotFound(data);
        }
        #endregion
       

        

    }
}